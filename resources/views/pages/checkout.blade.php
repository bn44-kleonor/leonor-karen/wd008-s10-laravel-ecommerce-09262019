<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use Session;
use App\Product;
 
class CartController extends Controller
{
    //
    public function addToCart(Request $request,$id)
    {
        //get quantity from form
        $quantity = $request->quantity;
        // dd($quantity);
        //check if there is a cart session
        if(Session::has("cart"))
        {
            //get data from cart session
            $cart = Session::get("cart");
        }
        //otherwise
        else
        {
            //initialize cart was an empty array
            $cart =[];
        }
 
        //check if added product is in cart session
        if(isset($cart[$id]))
        {
            //add to existing quantity through array push
            $cart[$id] += $quantity;
        }
        else
        {
            //assign wuantity to product (key-value-pair)
            $cart[$id] = $quantity;
        }
        //save changes to cart
        Session::put("cart",$cart);
        $product =Product::find($id);
 
        //redirect
        // return redirect()->back()->with("success","$product->name has been added to cart!");
 
        $count = collect(Session::get("cart"))->sum();
        return $count;
    }
}