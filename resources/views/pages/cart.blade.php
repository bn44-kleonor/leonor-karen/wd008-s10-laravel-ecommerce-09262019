@extends("layouts.app")
@section("content")
<!-- CART PAGE -->

	<div class="row">
		<div class="col">
			<div class="display-4">
				{{ $title }}
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col">
			<table class="table">
			  <thead class="thead-dark">
			    <tr>
			      <th scope="col"></th>
			      <th scope="col">Name</th>
			      <th scope="col">Price</th>
			      <th scope="col">Quantity</th>
			      <th scope="col">Subtotal</th>
			      <th scope="col">Action</th>
			    </tr>
			  </thead>
			  <tbody>
			  	@if(Session::has('cart'))
			  		<!-- PRODUCTS -->
			  		@foreach($cart_products as $product)
				    <tr>
				      <th scope="row">X</th>
				      <td>{{ $product->name }}</td>
				      <td>&#36; {{ $product->price }}</td>
				      <td>
				      	<div class="form-group">
				      		<input type="number" class="form-control update-quantity" min="1" value="{{ $product->quantity }}">
				      	</div>
				      </td>
				      <td>&#36; {{ $product->subtotal }}</td>
				      <td>
				      	<a href="#" class="btn btn-success">View</a>
				      </td>
				    </tr>
			  		@endforeach
			  	@endif
				<!-- TOTAL -->
					<tr>
						<td colspan="4" class="text-right">
							<h4>Total</h4>
						</td>
						<td colspan="4" class="text-right">
							<h4>&#36; {{ $product->total }}</h4>
						</td>
					</tr>
			  </tbody>
			</table>		
		</div>
	</div>
@endsection