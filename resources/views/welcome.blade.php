@extends("layouts.app")
@section("content")
{{-- dd($products) --}}
    <!-- PRODUCTS IN CARDS -->
    
    <div class="row">
    	@foreach($products as $product)
    	<div class="col">
		  <div class="card">
		    <img class="card-img-top" src="{{ $product->image }}" alt="Card image cap">
		    <div class="card-body">
		      <h5 class="card-title">{{ $product->name }}</h5>
		      <p class="card-text">{{ $product->description }}.</p>
		      <p class="card-text">&#36;{{ $product->price }}</p>
		    </div>
		  </div>
		  <!-- <button type="button" class="btn btn-primary"> -->
		  		<!-- ADD TO CART -->
		  	<div class="card-footer">
		  		<!-- <form method="POST" action="/addToCart/{{ $product->id }}"> -->
		  		<!-- <form method="POST" class="addToCart-form-{{$product->id}}"> -->
		  		<form method="POST" class="form-{{$product->id}}">
		  			@csrf
		  			<div class="form-group">
		  				<input type="number" name="quantity" class="form-control" min="1" max="{{ $product->stock }}" value="1">
		  			</div>
		  			<button type="button" class="btn btn-primary btn-block btn-addToCart" data-id="{{ $product->id }}" data-name="{{ $product->name }}">
		  				Add to Cart
		  			</button>
		  		</form>
		  	</div>
		  <!-- </button> -->
		</div>
		@endforeach
    </div>    
@endsection
