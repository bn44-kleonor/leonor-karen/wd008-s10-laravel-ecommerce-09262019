<?php

use Illuminate\Database\Seeder;
use App\Status;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Status::insert([
        	['name' => 'pending'],
        	['name' => 'cancelled'],
        	['name' => 'completed']
        ]);
    }
}
