<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::insert
        ([
        	[   'name' => 'broccoli', 
                'price' => 10, 
                'description' => 'description of brocolli', 
                'stock' => 100, 
                'category_id' => 1
            ],
        	[   'name' => 'beef', 
                'price' => 20, 
                'description' => 'description of beef', 
                'stock' => 200, 
                'category_id' => 2
            ],
        	[   'name' => 'salmon', 
                'price' => 30, 
                'description' => 'description of salmon', 
                'stock' => 300, 
                'category_id' => 3
            ],
        	[   'name' => 'mango', 
                'price' => 40, 
                'description' => 'description of mango', 
                'stock' => 400, 
                'category_id' => 4
            ],
            [   'name' => 'broccoli II', 
                'price' => 110, 
                'description' => 'description of brocolli II', 
                'stock' => 1100, 
                'category_id' => 1
            ],
            [   'name' => 'beef II', 
                'price' => 210, 
                'description' => 'description of beef II', 
                'stock' => 2100, 
                'category_id' => 2
            ],
            [   'name' => 'salmon II', 
                'price' => 310, 
                'description' => 'description of salmon II', 
                'stock' => 3100, 
                'category_id' => 3
            ],
            [   'name' => 'mango II', 
                'price' => 410, 
                'description' => 'description of mango II', 
                'stock' => 4100, 
                'category_id' => 4
            ],
        ]);
    }
}
