<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', 'PageController@welcome');


Route::get('/cart', 'PageController@cart');

Route::group(["middleware" => "auth"], function () {
	Route::get('/checkout', 'PageController@checkout');
	Route::get("/confirmation", "PageController@confirmation");
});

// Route::get('/products', 'ProductController@index')->middleware("auth");
Route::group(["middleware" => ["isAdmin", "auth"]], function() 
{
	Route::get("/products", "ProductController@index");
	Route::delete("/products/{id}", "ProductController@destroy");
	Route::get("products/{id}/edit", "ProductController@edit");
	Route::put("/products/{id}", "ProductController@update");
	Route::get("/products/create", "ProductController@create");
});

Route::post("addToCart/{id}", "CartController@addToCart");

