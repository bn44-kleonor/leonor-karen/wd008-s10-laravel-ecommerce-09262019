//delete product

document.addEventListener("click", function(e){
	//console.log(e.target.classList.contains("btn-delete-product"));

	//========= DELETE PRODUCT
	if(e.target.classList.contains("btn-delete-product"))
	{
		//alert('hello');

		let button = e.target;
		
		//get product_id
		let product_id = button.dataset.id;
		// alert(product_id);


		// get product name
		let product_name = button.dataset.name;
		//put product_name inside span
		let modal_text = document.querySelector("#modal_product_name");
		modal_text.innerHTML = product_name;
		

		//get the modal's form
		let modal_form = document.querySelector("#delete_modal_form");
		//set action attribute to modal form
		modal_form.setAttribute("action", `/products/${product_id}`);

	}

	//======== ADD TO CART
	if(e.target.classList.contains("btn-addToCart"))
	{
		let button = e.target;
		// console.log(button.previousElementSibling.children[0].value);
		if(button.previousElementSibling.children[0].value >= 1)
		{
			let product_name = button.dataset.name;
			let product_id = button.dataset.id;
			// let form = document.querySelector(`.addToCart-form-${product_id}`);
			let form = document.querySelector(`form[class="form-${product_id}"]`);

			// console.log(product_id);

			fetch(`/addToCart/${product_id}`, {
				method: "POST", 
				credentials: "same-origin",
				body: new FormData(form)
			})
				.then(function(response){
					return response.text();
				})
				.then(function(data_from_fetch){
					//console.log(data_from_fetch);
					//update badge count
					let badge = document.querySelector("#cart-count");
					badge.innerHTML = data_from_fetch;

					//display success message
					let alert_success = document.createElement("div");
					alert_success.setAttribute("class", "alert alert-success");
					alert_success.setAttribute("id", "alert_success");
					alert_success.textContent = `${product_name} has beed added to cart!`;
					let alert_container = document.querySelector('#alert_container');
					alert_container.append(alert_success);
				})
				.catch(function(error){
					console.log("Request failed", error);
				})
		}
		else
		{
			//alert("bawal");
			//from alerts.blade.php 	<div class="alert alert-danger" id="alert_error">
			let alert_error = document.createElement("div");
			alert_error.setAttribute("class", "alert alert-danger");
			alert_error.setAttribute("id", "alert_error");
			alert_error.textContent = "Please add a valid quantity";
			let alert_container = document.querySelector('#alert_container');
			alert_container.append(alert_error);
		}
	}
})
