<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Product;
use Session;

class PageController extends Controller
{
    //index method - returns welcome.blade.php with all products
    public function welcome () {
    	$products = Product::orderBy("stock", "desc")->paginate(3);
    	return view('welcome')->with('products', $products);
    }

    public function cart () {
        //create an array where we will push our product & quantity
        $cart_products = [];

        //set total 
        $total = 0;

        //check if cart session exists to iterate
        if(Session::has("cart"))
        {
            $cart = Session::get("cart");
            foreach($cart as $id => $quantity)  //"quantity" nasa 'Session'
            {
                //find the product via its id
                $product = Product::find($id);

                //add quantity attribute to product
                $product->quantity = $quantity;

                //add subtotal attribute to product
                $product->subtotal = $product->price * $quantity;

                //get total
                $total += $product->subtotal;

                //array push product to cart_products variable
                $cart_products[] = $product;
            }
        }

        $title = "My Cart";
    	return view("pages.cart", compact('cart_products', "total", "title"));
    }

    public function checkout () {
    	return view("pages.checkout");
    }

    public function confirmation () {
    	return view("pages.confirmation");
    }
}
